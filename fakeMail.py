import requests
from bs4 import BeautifulSoup
from selenium import webdriver
import time

headers = {
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36'
    }

def getFakeMail():
    url = 'https://email-fake.com/'
    req = requests.get(url)
    soup = BeautifulSoup(req.content, "html.parser")
    mail = soup.find("input", {"id": "userName"}).get('value')
    domain = soup.find("input", {"id": "domainName2"}).get('value')
    return f"{mail}@{domain}"

def getFakeMail_sec(driver):
    driver.execute_script("window.open('');")
    driver.switch_to.window(driver.window_handles[1])

    driver.get('https://tempail.com/en')
    driver.implicitly_wait(2)
    mail = driver.find_element_by_xpath("/html/body/section[1]/div[2]/div/div[2]/div/div/div/input").get_attribute('value')
    driver.switch_to.window(driver.window_handles[0])
    return mail


if __name__ == "__main__":
    driver = webdriver.Chrome(r'C:\Users\yaros\PycharmProjects\AutoReg\chromedriver.exe')
    print(getFakeMail_sec(driver))
