import time
from selenium import webdriver

def getInstVeriCode(driver):

    driver.switch_to.window(driver.window_handles[1])

    title = driver.title
    while True:
        if title == "Temp Mail - Temporary Email":
            driver.refresh()
            title = driver.title
            time.sleep(2)
        else:
            break

    code = driver.find_element_by_xpath("/html/body/section[2]/div/div/div/ul/li[2]/a/div[3]").text
    code = code.replace("is your Instagram code", "")

    driver.switch_to.window(driver.window_handles[0])
    return code[:-1]
    

def getInstVeriCodeDouble(mailName, domain, driver, oldCode):

    INST_CODE = 'https://email-fake.com/' + domain + '/' + mailName
    
    driver.execute_script("window.open('');")
    driver.switch_to.window(driver.window_handles[1])
    driver.get(INST_CODE)
    time.sleep(4)

    code = driver.find_element_by_xpath("/html/body/div[3]/div/div/div[1]/div[2]/a[1]/div[2]").text
    while oldCode == code:
        driver.refresh()
        print('Whait for new code!')
        time.sleep(1)
        code = driver.find_element_by_xpath("//*[@id='email-table']/div[2]/div[1]/div/h1").text
    
    codeNew = code[:6]
    driver.switch_to.window(driver.window_handles[0])
    return codeNew

if __name__ == "__main__":
    pass